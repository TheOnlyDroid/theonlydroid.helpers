﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TheOnlyDroid.Helpers
{
    public static class Attributes
    {
        [AttributeUsage(AttributeTargets.All)]
        public class DeveloperAttribute : Attribute
        {
            // Private fields.
            private string name;
            private string message;
            private string level;
            private bool reviewed;

            // This constructor defines three required parameters: name, message & level.

            public DeveloperAttribute(string name, string message, string level)
            {
                this.name = name;
                this.message = message;
                this.level = level;
                this.reviewed = false;
            }

            // Define Name property.
            // This is a read-only attribute.

            public virtual string Name
            {
                get { return name; }
            }

            // Define Message property
            // This is a read-only attribute

            public virtual string Message
            {
                get { return message; }
            }

            // Define Level property.
            // This is a read-only attribute.

            public virtual string Level
            {
                get { return level; }
            }

            // Define Reviewed property.
            // This is a read/write attribute.

            public virtual bool Reviewed
            {
                get { return reviewed; }
                set { reviewed = value; }
            }
        }
    }
}
