﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

/*
   Copyright 2016 TheOnlyDroid & Austin D & SizableTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace TheOnlyDroid.Helpers
{
    #region Math Helpers
    public class MathHelpers
    {
        /// <summary>
        /// Estimates/Calculates the distance between two points
        /// </summary>
        /// <returns>The distance between two point in two dimentional space and returns '0' if there isn't any or null</returns>
        /// <param name="X1">Position X_One in relative 2D Space</param>
        /// <param name="X2">Position X_Two in relative 2D Space</param>
        /// <param name="Y1">Position Y_One in relative 2D Space</param>
        /// <param name="Y2">Position Y_Two in relative 2D Space</param>
        public static int Distance2D(int X1, int X2, int Y1, int Y2)
        {
            //     ______________________
            //d = √ (x2-x1)^2 + (y2-y1)^2
            //
            return (int)Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2));
        }

        /// <summary>
        /// Estimates/Calculates the distance between two points
        /// </summary>
        /// <returns>The distance between two point in three dimentional space and returns '0' if there isn't any or null</returns>
        /// <param name="X1">Position X_One in relative 3D Space</param>
        /// <param name="X2">Position X_Two in relative 3D Space</param>
        /// <param name="Y1">Position Y_One in relative 3D Space</param>
        /// <param name="Y2">Position Y_Two in relative 3D Space</param>
        /// <param name="Z1">Position Z_One in relative 3D Space</param>
        /// <param name="Z2">Position Z_Two in relative 3D Space</param>
        public static int Distance3D(int X1, int X2, int Y1, int Y2, int Z1, int Z2)
        {
            //     __________________________________
            //d = √ (x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2
            //
            return (int)Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2) + Math.Pow((Z2 - Z1), 2));
        }

        // Basically functions that won't be used Ever and only exist because WHY THE FUCK NOT

        /// <summary>
        /// Calculates the surface (&/or) area of a given triangle
        /// </summary>
        /// <returns>The surface (&/or) area of the given triangle (represented as points*/doubles*</returns>
        /// <param name="A">Point A (First)</param>
        /// <param name="B">Point B (Second)</param>
        /// <param name="C">Point C (Third)</param>
        public static double GetTriangleArea(double a, double b, double c) //:: Note :: I should really add a Triangle Draw function to create a BitMap and use it as an image for shits and giggles.
        {
            double s = (a + b + c) / 2;
            return Math.Sqrt(s * (s - a) * (s - b) * (s - c));
        }

    }
    #endregion

    #region Drawing Helpers
    public class DrawingHelpers
    {
        // Nothing currently lives here, soon my child... Soon.
    }
    #endregion

    #region String Helpers
    public class StringHelpers
    {   
        /// <summary>
        /// Split the source string by a delimeter at a specific location
        /// </summary>
        /// <returns>A string before the delimeter location or null if fail</returns>
        /// <param name="source">The string to split</param>
        /// <param name="delimeter">char delimeter.</param>
        /// <param name="index">The delimeter count</param>
        public static string SplitPortion(string source, char delimeter, int index)
        {
            if (string.IsNullOrEmpty(source) == true || string.IsNullOrEmpty(source) == true)
                return null;

            int delimeterCount = 0; // Increases by one each time delimeter is found
            bool foundDelimeter = false;
            StringBuilder returnValue = new StringBuilder();

            // loop through the string one character at a time
            for (int i = 0; i < source.Length; i++)
            {
                // if index is set to zero then build string til it encounters the first delimeter!
                if (index == 0)
                {
                    if (source[i] == delimeter) // found, break out
                    {
                        break;
                    }
                    else
                    {
                        returnValue.Append(source[i]);
                    }
                }
                // if delimeter was found on a previous character build string til we encounter the next one
                else if (foundDelimeter == true)
                {
                    if (source[i] != delimeter)
                    {
                        returnValue.Append(source[i]);
                    }
                    else
                    {
                        break;
                    }
                }
                // if delimeter is found increase the count
                else if (source[i] == delimeter)
                {
                    delimeterCount++;
                    if (delimeterCount == index)
                    {
                        foundDelimeter = true;
                    }
                }
            }
            return returnValue.ToString();
        }

        /// <summary>
        /// Parses a string before the last encountered delimeter.
        /// </summary>
        /// <returns>The string after the first delimeter encountered or null if not found.</returns>
        /// <param name="source">Source string</param>
        /// <param name="delimeter">Delimeter string</param>
        public static string BeforeLast(string source, string delimeter)
        {
            if (string.IsNullOrEmpty(source) == true || string.IsNullOrEmpty(source) == true)
                return null;

            int location = source.LastIndexOf(delimeter);
            if (location > -1)
            {
                return source.Substring(0, location);
            }
            return null;
        }

        /// <summary>
        /// Parses a string after the last encountered delimeter.
        /// </summary>
        /// <returns>The string after the last delimeter encountered or null if not found.</returns>
        /// <param name="source">Source string</param>
        /// <param name="delimeter">Delimeter string</param>
        public static string AfterLast(string source, string delimeter)
        {
            if (string.IsNullOrEmpty(source) == true || string.IsNullOrEmpty(source) == true)
                return null;

            int location = source.LastIndexOf(delimeter);
            if (location > -1)
            {
                int startPosition = location + delimeter.Length;
                int length = source.Length - startPosition;
                return source.Substring(startPosition, length);
            }
            return null;
        }

        /// <summary>
        /// Parses a string before the first encountered delimeter.
        /// </summary>
        /// <returns>The string before the first delimeter encountered or null if not found.</returns>
        /// <param name="source">Source string</param>
        /// <param name="delimeter">Delimeter string</param>
        public static string BeforeFirst(string source, string delimeter)
        {
            if (string.IsNullOrEmpty(source) == true || string.IsNullOrEmpty(source) == true)
                return null;

            int location = source.IndexOf(delimeter);
            if (location > -1)
            {
                return source.Substring(0, location);
            }
            return null;
        }

        /// <summary>
        /// Parses a string after the first encountered delimeter.
        /// </summary>
        /// <returns>The string after the first delimeter encountered or null if not found.</returns>
        /// <param name="source">Source string</param>
        /// <param name="delimeter">Delimeter string</param>
        public static string AfterFirst(string source, string delimeter)
        {
            if (string.IsNullOrEmpty(source) == true || string.IsNullOrEmpty(source) == true)
                return null;

            int location = source.IndexOf(delimeter);
            if (location > -1)
            {
                int startPosition = location + delimeter.Length;
                int length = source.Length - startPosition;
                return source.Substring(startPosition, length);
            }
            return null;
        }


    }
    #endregion

    #region UniqueID
    public abstract class UniqueID
    {
        /// <summary>
        /// Generate a generated ID from the date and time
        /// </summary>
        private static String generatedID
        {
            // create a read only unique ID
            get { return String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond); }
        }

        /// <summary>
        /// Get a unique ID for use with automated file handling
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static String ReturnUniqueID(EUniqueIDFormat format)
        {
            // return value
            String modifiedID = "";
            // create a return value
            String capturedID = UniqueID.generatedID;

            // select which format the ID should take
            switch (format)
            {
                // text only
                case (EUniqueIDFormat.Text):
                    {
                        // work through each character
                        foreach (Char idElement in capturedID)
                        {
                            // expose its int value, + 16 for ASCII offset to captials
                            modifiedID += (Char)((Int32)idElement + 17);
                        }
                        break;
                    }
                // text and numeric
                case (EUniqueIDFormat.Combined):
                    {
                        // work through each character
                        foreach (Char idElement in capturedID)
                        {
                            // if the characters ascii value is even
                            if ((Int32)idElement % 2 == 0)
                            {
                                // expose its int value, + 16 for ASCII offset to captials
                                modifiedID += (Char)((Int32)idElement + 17);
                            }
                            // element must be odd
                            else
                            {
                                // just add the element as its numeric form
                                modifiedID += idElement;
                            }
                        }
                        break;
                    }
                case (EUniqueIDFormat.Numeric):
                    {
                        // just set ID
                        modifiedID = capturedID;
                        break;
                    }
                default:
                    {
                        // throw excetion
                        throw new Exception("Unique format type is invalid");
                    }
            }

            // retun ID
            return modifiedID;
        }
    }

    /// <summary>
    /// Enumeration of UniqueID types
    /// </summary>
    public enum EUniqueIDFormat { Numeric = 0, Text, Combined }
    #endregion

    #region Misc
    public static class Misc
    {
        public static IEnumerable<DateTime> BusinessDays(this DateTime date)
        {
            return from d in DaysInMonth(date)
                   let day = new DateTime(date.Year, date.Month, d)
                   let dow = day.DayOfWeek
                   where
                       !dow.CompareTo(DayOfWeek.Saturday).Equals(0)
                       && !dow.CompareTo(DayOfWeek.Sunday).Equals(0)
                   select day;
        }

        private static IEnumerable<int> DaysInMonth(DateTime date)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(date.Year, date.Month));
        }
    }
    #endregion

    #region Cryptography Helpers
    public class CryptographyHelpers
    {
        private static byte[] SALT = { 34, 65, 11, 12, 16, 0, 65, 128, 92, 72, 65, 23, 87, 11, 10, 8 };

        private enum CryptProc { ENCRYPT, DECRYPT };

        /// <summary>
        /// Performs either an encryption or decryption <thanks woodsy for finding a typo, because I'm blind as a fucking bat>
        /// </summary>
        /// <param name="plain">Unencrypted byte array
        /// <param name="password">Password to be used
        /// <param name="iterations">Number of iterations hash algorithm uses
        /// <param name="cryptproc">Process to be performed
        /// <returns>Results of process in the form of a byte array</returns>
        private static byte[] CryptBytes(byte[] plain, string password, int iterations, CryptProc cryptproc)
        {
            //Create our key from the password provided
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, SALT, "SHA512", iterations);

            //We'll be using 3DES
            TripleDES des = TripleDES.Create();
            des.Key = pdb.GetBytes(24);
            des.IV = pdb.GetBytes(8);

            MemoryStream memstream = new MemoryStream();

            ICryptoTransform cryptor = (cryptproc == CryptProc.ENCRYPT) ? des.CreateEncryptor() : des.CreateDecryptor();

            CryptoStream cryptostream = new CryptoStream(memstream, cryptor, CryptoStreamMode.Write);
            cryptostream.Write(plain, 0, plain.Length); //write finished product to our MemoryStream

            cryptostream.Close();

            return memstream.ToArray();
        }

        /// <summary>
        /// Encrypts byte arrays
        /// </summary>
        /// <param name="plain">Unencrypted byte array
        /// <param name="password">Password to be used
        /// <param name="iterations">Number of iterations hash algorithm uses
        /// <returns>Encypted byte array</returns>
        public static byte[] EncryptBytes(byte[] plain, string password, int iterations)
        {
            return CryptBytes(plain, password, 2, CryptProc.ENCRYPT);
        }

        /// <summary>
        /// Decrypts byte arrays
        /// </summary>
        /// <param name="plain">Unencrypted byte array
        /// <param name="password">Password to be used
        /// <param name="iterations">Number of iterations hash algorithm uses
        /// <returns>Decrypted byte array</returns>
        public static byte[] DecryptBytes(byte[] plain, string password, int iterations)
        {
            return CryptBytes(plain, password, 2, CryptProc.DECRYPT);
        }
    }
    #endregion
}