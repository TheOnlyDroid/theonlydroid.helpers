﻿using MySql.Data.MySqlClient;

/*
   Copyright 2016 TheOnlyDroid & Austin D & SizableTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace TheOnlyDroid.Helpers.MySQL
{
    public class MySQL
    {
        #region (MySQL) Insert
        /// <summary>
        /// Performs either an encryption or 
        /// </summary>
        /// <param name="DbConnection">The passed connection "varible"/"state"
        /// <param name="query">The "manual" query to be inserted
        /// <returns>A true/false variable based on success or falure of the query</returns>
        /// 
        /// <notes>
        ///     <note id="1">Does not require the "INSERT INTO" portion of the query, this is already ready before hand</note>
        /// </notes>
        ///
        // MySQL_Insert(MySqlConnection, "userinfo (name,age,email) VALUES('Jake','32','test@test.org'); 
        public bool MySQL_Insert(MySqlConnection DbConnection, string query)
        {
            string BuiltQuery = $"INSERT INTO {query}";

            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return true;
            }
        }

        /// <summary>
        /// Performs either an encryption or 
        /// </summary> 
        /// <param name="DbConnection">The passed connection "varible"/"state"
        /// <param name="table">The table to be inserted into
        /// <param name="columns">The columns you wish to add data too (EG: Ones without default values)
        /// <param name="values">The values of the columns you wish to add data too
        /// <returns>A true/false variable based on success or falure of the query</returns>
        /// 
        // MySQL_Insert(MySqlConnection, "userinfo", "name,age,email", "'Jake','32','test@test.org'");
        public bool MySQL_Insert(MySqlConnection DbConnection, string table, string columns, string values)
        {
            string BuiltQuery = $"INSERT INTO {table} ({columns}) VALUES({values})";

            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return true;
            }
        }
        #endregion

        #region (MySQL) Update
        public bool MySQL_Update(MySqlConnection DbConnection, string query)
        {
            string BuiltQuery = $"UPDATE {query}";
            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return false;
            }
        }

        public bool MySQL_Update(MySqlConnection DbConnection, string table, string set_data, string WhereValue)
        {
            string BuiltQuery = $"UPDATE {table} SET {set_data} WHERE {WhereValue}";

            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return false;
            }
        }
        #endregion

        #region (MySQL) Delete
        public bool MySQL_Delete(MySqlConnection DbConnection, string query)
        {
            string BuiltQuery = $"DELETE {query}";

            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;

            }
            catch (MySqlException) {
                return false;
            }
        }

        public bool MySQL_Delete(MySqlConnection DbConnection,string table, string WhereValue)
        {
            string BuiltQuery = $"DELETE FROM {table} WHERE {WhereValue}";

            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;

            }
            catch (MySqlException) {
                return false;
            }
        }
        #endregion

        #region (MySQL) Delete_All
        [Attributes.Developer("TheOnlyDroid", "Cannot use this function above MySQL/phpmyadmin version 5.1(/7)", "Developer/Project Lead", Reviewed = false)]
        public bool MySQL_DeleteAll(MySqlConnection DbConnection, string table)
        {
            string BuiltQuery = $"DELETE * FROM {table}";
            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return false;
            }
        }

        [Attributes.Developer("TheOnlyDroid", "Cannot use this function above MySQL/phpmyadmin version 5.1(/7)", "Developer/Project Lead", Reviewed = false)]
        public bool MySQL_DeleteAll(MySqlConnection DbConnection, string table, string WhereValue)
        {
            string BuiltQuery = $"DELETE * FROM {table} WHERE {WhereValue}";
            try
            {
                DbConnection.OpenAsync();
                MySqlCommand Local_Command_MySQL = new MySqlCommand(BuiltQuery, DbConnection);
                Local_Command_MySQL.ExecuteNonQueryAsync();
                DbConnection.CloseAsync();
                return true;
            }
            catch (MySqlException) {
                return false;
            }
        }
        #endregion

        #region (MySQL) Other
        /*
        public void MySQL_Backup(string UID, string Password, string Server, string Database, string BaseFolder)
        {
            try
            {
                DateTime Time = DateTime.Now;
                int year = Time.Year;
                int month = Time.Month;
                int day = Time.Day;
                int hour = Time.Hour;
                int minute = Time.Minute;
                int second = Time.Second;
                int millisecond = Time.Millisecond;

                //Save file to C:\ with the current date as a filename
                string path;
                path = $@"{BaseFolder}\Database\Backups\{year}-{month}-{day}-{hour}-{minute}-{second}-{millisecond}.sql";
                StreamWriter file = new StreamWriter(path);


                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "mysqldump";
                psi.RedirectStandardInput = false;
                psi.RedirectStandardOutput = true;
                psi.Arguments = string.Format(@"-u{0} -p{1} -h{2} {3}",
                    UID, Password,Server, Database);
                psi.UseShellExecute = false;

                Process process = Process.Start(psi);

                string output;
                output = process.StandardOutput.ReadToEnd();
                file.WriteLine(output);
                process.WaitForExit();
                file.Close();
                process.Close();
            }
            catch (IOException)
            {
            }
        }*/
        #endregion

        public class Readers
        {

        }
    }
}

#region Useful Functions (That aren't used)
/* 
USEFUL MySQL Functions ::

public List< string >[] Select()
{
    string query = "SELECT * FROM tableinfo";

    //Create a list to store the result
    List< string >[] list = new List< string >[3];
    list[0] = new List< string >();
    list[1] = new List< string >();
    list[2] = new List< string >();

    //Open connection
    if (this.OpenConnection() == true)
    {
        //Create Command
        MySqlCommand cmd = new MySqlCommand(query, connection);
        //Create a data reader and Execute the command
        MySqlDataReader dataReader = cmd.ExecuteReader();
        
        //Read the data and store them in the list
        while (dataReader.Read())
        {
            list[0].Add(dataReader["id"] + "");
            list[1].Add(dataReader["name"] + "");
            list[2].Add(dataReader["age"] + "");
        }

        //close Data Reader
        dataReader.Close();

        //close Connection
        this.CloseConnection();

        //return list to be displayed
        return list;
    }
    else
    {
        return list;
    }
}
*/
#endregion