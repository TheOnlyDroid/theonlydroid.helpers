﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheOnlyDroid.Helpers
{
    public class ProjectDarter
    {
        public class DataReaders
        {
            internal string[] FileFormats = { ".dttl", ".dxml", ".darter", ".dart", ".dlog" };
            internal string[] CryptKeys = { "ECC83CD3FB57E73E8CFEDB6563CB6", "C485188A116513BD13CCCCA77FD66", "A197A21B231F64B4AC6E76D477F74", "154934D134DC2B23D662C81C78B91", "C93A7BC59411FC8BEA63CACC8541D" };
        }

        public class DataEncryption
        {
            internal string[] FileFormats = { ".dttl", ".dxml", ".darter", ".dart", ".dlog" };
            internal string[] CryptKeys = { "ECC83CD3FB57E73E8CFEDB6563CB6", "C485188A116513BD13CCCCA77FD66", "A197A21B231F64B4AC6E76D477F74", "154934D134DC2B23D662C81C78B91", "C93A7BC59411FC8BEA63CACC8541D" };
        }

        public class DataDecryption
        {
            internal string[] FileFormats = { ".dttl", ".dxml", ".darter", ".dart", ".dlog" };
            internal string[] CryptKeys = { "ECC83CD3FB57E73E8CFEDB6563CB6", "C485188A116513BD13CCCCA77FD66", "A197A21B231F64B4AC6E76D477F74", "154934D134DC2B23D662C81C78B91", "C93A7BC59411FC8BEA63CACC8541D" };
        }
    }
}
